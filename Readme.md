# Dao Title and Warrior Name Generator

This is a little name generator library for Dao and Warrior
based on Chinese Xuanxia novels.

## Install

In your `composer.json` add following:

`"repositories": [ {"type": "vcs", "url": "https://gitlab.com/eirworks/daonames"} ]`

then do this command via command line: 

`$ composer require hugehence/daonames`

or simply manually write `hugehence/daonames` in require section.

## Basic usage

`\Dao\Names::generate()`

will returns something like

`soaring dragon`, `immortal phoenix`, `red river`, etc.