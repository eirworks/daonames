<?php

namespace Dao;

class Random {


    public static function randomFromArray(array $array)
    {
        $size = count($array);

        return $array[rand(0, $size-1)];
    }

}