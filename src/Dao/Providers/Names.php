<?php

namespace Dao\Providers;

class Names {


    protected static $objects = [
        'river', 'brook', 'sky', 'phoenix', 'mount', 'lake',
        'mist', 'thunder', 'bun', 'dragon', 'galaxia',
    ];

    protected static $adjectives = [
        'steaming', 'cleansing', 'blue', 'red', 'yellow', 'purple',
        'coating', 'soaring', 'invicible',
    ];

}