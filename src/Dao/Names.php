<?php

namespace Dao;

class Names {


    public static function generate()
    {
        $objects = \Dao\Providers\Names::objects();
        $adjectives = \Dao\Providers\Names::adjectives();

        return ucwords(\Dao\Random::randomFromArray($adjectives)." ".\Dao\Random::randomFromArray($objects));
    }

}